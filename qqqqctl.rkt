#!/usr/bin/env racket
#lang racket/base

(require racket/unix-socket racket/cmdline)

(define sockfile "/tmp/qqqq.sock")

(define msg 
  (command-line
    #:args (m) 
    m))

(define-values (in out) (unix-socket-connect sockfile))
(write msg out)
